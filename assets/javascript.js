function registerfunction(){
	var username = $('#username').val();
	var email = $('#email').val();
	var password = $('#password').val();
	var passconf = $('#passconf').val();
	// var base_url = <?php echo base_url(); ?>;
	$.ajax({
		'url' : 'http://localhost/task/index.php/user/insertregistration',
		'type' :'POST',
		'data' : {'username':username,'email':email,'password':password,'passconf':passconf},
		success:function(data)
		{
			$('#message').html(data);
		}
	});
}


function login(){
	var email = $('#email').val();
	var password = $('#password').val();
	$.ajax({
		'url' : 'http://localhost/task/index.php/user/checklogin',
		'type' :'POST',
		'data' : {'email':email,'password':password},
		success:function(data)
		{
			console.log(data);
			if(data == "yes"){
				window.location = "http://localhost/task/index.php/user";
			}else{
				$('#message').html("login and password not match");
			}
		}
	});
}

function deletefun(elem){
	var id = $(elem).parent().siblings(":first").text();
	$.ajax({
		'url' : 'http://localhost/task/index.php/user/deleteuser',
		'type' :'POST',
		'data' : {'id':id},
		success:function(data)
		{
			if(data == "yes"){
				$(elem).closest('tr').remove();
				$('#message').html("Successfully deleted");
			}else{
				$('#message').html("Some error");
			}
		}
	});
// http://localhost/task/
}


function search(){
	$('#waitmessage').html('wait while we extract all status.....');
	var search = $('#search').val();
	$.ajax({
		'url' : 'http://localhost/task/index.php/user/searchdata',
		'type' :'POST',
		'data' : {'search':search},
		success:function(data)
		{
			$('#waitmessage').html('results are displayed');
			$('#message').html(data);			
		}
	});
}