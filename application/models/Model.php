<?php
/*
*  Add Class UserModel to make database query and access from user Controller class
*/

error_reporting(E_ALL);

class model extends CI_Model 
{
	/*
	* Make constructor of model class
	*/
	function __construct()
	{
		parent::__construct();
	}

	function getalluser(){
		$sql = "SELECT * from user where role != 1"; // role = 1 for admin
		$query= $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function login($email, $password){
		$sql = "SELECT * from user where email = '".$email."' AND password = '".$password."'";
		$query= $this->db->query($sql);
		if($query->num_rows() > 0){
			$role = $query->row()->role;
			if($role != NULL && $role == 1){
				$role = "yes";
			}
			else{
				$role= "no";
			}
			$sessionarray = array('login'=>$email, 'islogged'=>'yes','isadmin'=>$role);
			$this->session->set_userdata($sessionarray);
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function checkuser_exists($email){
		$checkemail = "SELECT * from user where email = '".$email."'";
		$q  = $this->db->query($checkemail);
		if($q->num_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insertuser($username, $email, $password,$role,$date){
		$sql = "INSERT INTO user (username,email, password, photo,role,date_created) values('".$username."', '".$email."', '".$password."','',".$role.",'".$date."')";
		$insertquery = $this->db->query($sql);
		if($insertquery){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	function deleteuser($id){
		$sql = "DELETE from user where id = ".$id;
		$q = $this->db->query($sql);
		if($q){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	function queryTwitter($search) 
	{
	    $url = "https://api.twitter.com/1.1/search/tweets.json";
	    if($search != "")
	        $search = "#".$search;
	    $query = array( 'count' => 100, 'q' => urlencode($search), "result_type" => "recent");
	    $oauth_access_token = "873237228-mz5XuubQBmf89LfUnWvgYFGQk0Q4Bgz2L4S2bxXs";
	    $oauth_access_token_secret = "PjM5sXCvFwxVwsz9dqe6euJSvOBil5q8G8SKbiilrk2N2";
	    $consumer_key = "O1Vf8Tc3W7a6URvCcK45H936L";
	    $consumer_secret = "GogSNKSdliy7ZXzNkQ0eHhtXBLaxLTeBOJCzkN2pVJq1oaCnhC";

	    $oauth = array(
	                    'oauth_consumer_key' => $consumer_key,
	                    'oauth_nonce' => time(),
	                    'oauth_signature_method' => 'HMAC-SHA1',
	                    'oauth_token' => $oauth_access_token,
	                    'oauth_timestamp' => time(),
	                    'oauth_version' => '1.0');

	    $base_params = empty($query) ? $oauth : array_merge($query,$oauth);
	    $base_info = $this->buildBaseString($url, 'GET', $base_params);
	    $url = empty($query) ? $url : $url . "?" . http_build_query($query);

	    $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
	    $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
	    $oauth['oauth_signature'] = $oauth_signature;

	    $header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
	    $options = array( CURLOPT_HTTPHEADER => $header,
	                      CURLOPT_HEADER => false,
	                      CURLOPT_URL => $url,
	                      CURLOPT_RETURNTRANSFER => true,
	                      CURLOPT_SSL_VERIFYPEER => false);

	    $feed = curl_init();
	    curl_setopt_array($feed, $options);
	    $json = curl_exec($feed);
	    curl_close($feed);
	    return  json_decode($json);
	}

	function buildBaseString($baseURI, $method, $params)
	{
	    $r = array(); 
	    ksort($params);
	    foreach($params as $key=>$value){
	        $r[] = "$key=" . rawurlencode($value); 
	    }
	    return $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r)); 
	}

	function buildAuthorizationHeader($oauth)
	{
	    $r = 'Authorization: OAuth '; 
	    $values = array(); 
	    foreach($oauth as $key=>$value)
	        $values[] = "$key=\"" . rawurlencode($value) . "\""; 
	    $r .= implode(', ', $values); 
	    return $r; 
	}

}