<?php
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M'); 
class user extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
        $this->load->helper('url');
		$this->load->library('session');
		$this->load->model('Model');
	}

	function index(){
		if($this->session->userdata('islogged') != "yes"){
			echo "here";
			header("Location: User/login");
			die;
		}
		$data['title'] = "Login";
		if($this->session->userdata('isadmin') == "yes"){
			$data['users'] = $this->Model->getalluser();
		}
		$this->load->view('pages/header',$data);
		if($this->session->userdata('isadmin') == "yes"){
			$this->load->view('pages/userdetails',$data);
		}else{
			$this->load->view('pages/userindex',$data);
		}
		$this->load->view('pages/footer',$data);
	}
	function login(){
		$this->load->helper('url');
		$this->load->view('pages/login');
	}
	function registration(){
		$this->load->helper('url');
		$this->load->view('pages/registration');
	}


	function adduser(){
		$data['title'] = "Add User";
		$this->load->view('pages/header',$data);
		$this->load->view('pages/adduser',$data);
		$this->load->view('pages/footer',$data);
	}


	function check(){
		$sql = "SELECT * from user";
		$q= $this->db->query($sql);
		$res = $q->result();
		var_dump($res);
	}

	function checklogin(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$response = $this->Model->login($email, $password);
		if($response){
			echo "yes";
		}
		else{
			echo "no";
		}
	}

	function insertregistration(){
		
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$confpassword = $this->input->post('passconf');
		$image = $this->input->post('image');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|min_length[6]');
		$this->form_validation->set_rules('email', 'email', 'required|valid_email');

		if ($this->form_validation->run() == FALSE)
        {
        		echo validation_errors();
        		// header("Location: http://127.0.0.1/task/index.php/User/registration");
				// $this->load->view('pages/registration');
                exit;
        }
        elseif($password != $confpassword){
        		echo "password and conf. password not matched";
				$this->load->view('pages/registration');
                exit;
        }
		else{
			$role =0;
			$checkemail = $this->Model->checkuser_exists($email);
			if($checkemail){
				echo "mail id already exists";
			}else{
				$date = date('d M Y');
				$insertdata = $this->Model->insertuser($username, $email, $password,$role,$date);
				if($insertdata){
					echo "successfully submitted";
				}
				else{
					echo "failed";
				}				
			}
		}
	}

	function deleteuser(){
		$id = $this->input->post('id');
		$delete = $this->Model->deleteuser($id);
		if($delete){
			echo "yes";
		}
		else{
			echo "no";
		}
	}

	function logout()
	{

	    $user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
				$this->session->unset_userdata($key);
            }
        }
	    $this->session->sess_destroy();
	    header("Location: http://localhost/task/index.php/User/login");
	}

	function searchdata(){
		$search = $this->input->post('search');
		$query = (array)$this->Model->queryTwitter($search);
		$data = array();
		foreach ($query as $key => $value) {
			foreach ($value as $key2 => $value2) {
				$ar3 = (array)$value2;
				if(isset($ar3['text'])){
					$data['text'][] = $ar3['text'];
				}
				foreach ($ar3 as $key3 => $value3) {
					$textarray =(array)$value3;
					if(isset($textarray['text'])){
						$data['text'][] = $textarray['text'];
					}
				}
			}
		}
		if($data!=NULL ){
			$str = "<table id='example' class='table table-striped table-bordered'><thead><th>ID</th><th>Message</th></thead><tbody>";
			$i =1;
			foreach ($data['text'] as $key => $value) {
				if($i <80){
					$str .= "<tr><td> ".$i."</td><td>".$value."</td></tr>";
				}
				$i++;
			}
			$str .="</tbody></table>";
		}
		else{
			$str = "No data found";
		}
		echo $str;

	}

}