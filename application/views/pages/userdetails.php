 
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css"/>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

<script src="<?php echo base_url('assets/javascript.js') ?>"></script>


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Forms</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Basic Form Elements
                        </div>
                        <div class="panel-body">
                            <div class="row">
                              <div id="message" style="color:RED"></div>
                                <div class="col-lg-12">
                                     <table id="example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                                        <thead>
                                          <tr>
                                            <th hidden>id</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Delete</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($users as $key => $value){ ?>
                                          <tr>
                                            <td hidden><?php echo $value['id']; ?></td>
                                            <td><?php echo $value['username']; ?></td>
                                            <td><?php echo $value['email']; ?></td>
                                            <td><input type="button" name="delete" value="Delete" onclick="deletefun(this);" />
                                            </td>
                                          </tr>
                                          <?php } ?>
                                        </tbody>
                                      </table>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
