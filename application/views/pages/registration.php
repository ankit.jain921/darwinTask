<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Registration</title>

    <!-- Bootstrap Core CSS -->
    

    <link href="<?php echo 'http://localhost/task/assets/bootstrap/bower_components/bootstrap/dist/css/bootstrap.min.css'; ?>" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo 'http://localhost/task/assets/bootstrap/bower_components/metisMenu/dist/metisMenu.min.css'; ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo 'http://localhost/task/assets/bootstrap/dist/css/sb-admin-2.css'; ?>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo 'http://localhost/task/assets/bootstrap/bower_components/font-awesome/css/font-awesome.min.css'; ?>" rel="stylesheet" type="text/css">
    <!-- jQuery -->
    <script src="<?php echo 'http://localhost/task/assets/bootstrap/bower_components/jquery/dist/jquery.min.js'; ?>"></script>
    <script src="<?php echo 'http://localhost/task/assets/javascript.js'; ?>"></script>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Registration </h3>
                    </div>
                    <div class="panel-body">
                        <div id="message" style="color:RED"></div>
                            <fieldset>
                                 <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="username" id="username" type="text" autofocus required>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" id="email" type="email" required>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" id="password" type="password" value="" required>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Conf. Password" name="passconf" id="passconf" type="password" value="" required>
                                </div>
                                <div class="form-group">
                                    <label>Upload Image</label>
                                    <input type="file">
                                </div>

                                <input type="submit" onclick="registerfunction();" id="submitdetail" class="btn btn-lg btn-success btn-block" value="Registration">
                                <a href="http://localhost/task/index.php/user" class="btn btn-lg btn-success btn-block">Login</a>
                            </fieldset>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo 'http://localhost/task/assets/bootstrap/bower_components/bootstrap/dist/js/bootstrap.min.js'; ?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo 'http://localhost/task/assets/bootstrap/bower_components/metisMenu/dist/metisMenu.min.js'; ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo 'http://localhost/task/assets/bootstrap/dist/js/sb-admin-2.js'; ?>"></script>

</body>

</html>
